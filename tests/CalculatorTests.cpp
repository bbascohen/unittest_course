#define CATCH_CONFIG_MAIN

#include <catch2/catch.hpp>
#include "../Calculator.h"

TEST_CASE("Adding two numbers returns sum") {
    Calculator calculator;

    int result = calculator.add(1, 2);

    REQUIRE(result == 3);
}



