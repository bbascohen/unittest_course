# Unit testing course - setup environment
## Prerequisites
- [git](https://github.com/git-for-windows/git/releases/download/v2.33.0.windows.2/Git-2.33.0.2-64-bit.exe)
- [CMake](https://github.com/Kitware/CMake/releases/download/v3.21.3/cmake-3.21.3-windows-x86_64.msi)
- [conan](https://github.com/conan-io/conan/releases/latest/download/conan-win-64.exe)

.... Download and install missing apps.
## steps - from commandline
1. Clone repository  -
    - Go to [project page](https://bitbucket.org/bbascohen/unittest_course/src/main/)
    - Click `Clone` - top right.
    - You now have clone command in your clipboard.
    - Paste and run in git bash.
   
4. `cd unittest_course`
5. Run `conan install -if [binary path (where to build)] .`

## steps - from Clion

1. Go to Settings -> plugins - search for conan in marketplace, install it and restart Clion.
  
  ![conan.png](conan.png)
  
2. Clone repository -
   - Go to [project page](https://bitbucket.org/bbascohen/unittest_course/src/main/)
   - Click `Clone` - top right.
   - You now have clone command in your clipboard
   - Open clone -
     
      ![clone.png](clone.png)
        
   - Paste it in -
     
       ![img.png](clone1.png)
       
   - Click clone.
     
     ![img_2.png](OpenProj.png)
       
   - Click ok
3. On lower tabs: you will find Conan tab, click it -
  
    ![img_1.png](ConanTab.png)
    
4. Click on Match profile - and choose default on `Conan Profile`
  
    ![img_3.png](match.png)
  
5. Click on `Update And Install`
  
   ![img_4.png](UpandInst.png)
  
  
