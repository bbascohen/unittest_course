#ifndef SIMPLECALCULATOR_CALCULATOR_H
#define SIMPLECALCULATOR_CALCULATOR_H

class Calculator {
public:
    int add(int a, int b)
    {
        return a + b;
    }

    int substract(int a, int b)
    {
        return a - b;
    }

    int multiply(int a, int b)
    {
        return a * b;
    }

    int divide (int a, int b)
    {
        return a / b;
    }
};

#endif //SIMPLECALCULATOR_CALCULATOR_H
